// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot.
// Execute a function that will return an array from the dealer data
// containing only the car years and log the result in the console as it was returned.

const isValidInventory = require("./isValidInventory");
const problem4 = (inventory) => {
  const getYearOfAllVehicleModels = (inventory) => {
    let arrayOfYears = [];

    for (let vehicle = 0; vehicle < inventory.length; vehicle++) {
      arrayOfYears.push(inventory[vehicle].car_year);
    }

    return arrayOfYears;
  };

  if (isValidInventory(inventory)) {
    return getYearOfAllVehicleModels(inventory);
  }
  return [];
};

module.exports = problem4;
