const inventory = require("./inventory.js");
const problem1 = require("./problem1");
const problem2 = require("./problem2");
const problem3 = require("./problem3");
const problem4 = require("./problem4");
const problem5 = require("./problem5");
const problem6 = require("./problem6");

const id_key = 14;
const year = 1999;
const vehicleMakeArray = ["BMW", "Audi"];

const runProblem1 = problem1(inventory, id_key);
const runProblem2 = problem2(inventory);
const runProblem3 = problem3(inventory);
const runProblem4 = problem4(inventory);
const runProblem5 = problem5(problem4(inventory), year);
const runProblem6 = JSON.stringify(problem6(inventory, vehicleMakeArray));

console.log("\n\n----------Problem #1:----------\n");
console.log(runProblem1);
console.log("\n\n----------Problem #2:----------\n");
console.log(runProblem2);
console.log("\n\n----------Problem #3:----------\n");
console.log(runProblem3);
console.log("\n\n----------Problem #4:----------\n");
console.log(runProblem4);
console.log("\n\n----------Problem #5:----------\n");
console.log(runProblem5);
console.log("\n\n----------Problem #6:----------\n");
console.log(runProblem6);
