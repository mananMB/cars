const expect = require("chai").expect;
const problem3 = require("../problem3");
const inventory = require("../inventory.js");

const expectedMatchResult = inventory.sort((a, b) => {
  let fa = a.car_model.toLowerCase();
  let fb = b.car_model.toUpperCase();
  if (fa < fb) {
    return -1;
  }
  if (fa > fb) {
    return 1;
  }
  return 0;
});
const expectedNoMatchResult = [];
describe("Test problem #3", () => {
  it("returns inventory sorted by of last vehicle in inventory when valid arguments are provided", () => {
    const result = problem3(inventory);
    expect(result).deep.equal(expectedMatchResult);
  });
  it("returns inventory as it is if it only contains one item when valid arguments are provided", () => {
    const result = problem3([inventory[0]]);
    expect(result).deep.equal([inventory[0]]);
  });
  it("returns an empty array when inventory is empty", () => {
    const result = problem3([]);
    expect(result).deep.equal(expectedNoMatchResult);
  });
  it("returns an empty array when inventory is not and array of objects", () => {
    const result = problem3({ hello: "car" });
    expect(result).deep.equal(expectedNoMatchResult);
  });
  it("returns an empty array when inventory array has invalid object", () => {
    const result = problem3([{ hello: "car" }]);
    expect(result).deep.equal(expectedNoMatchResult);
  });
  it("returns an empty array when arguments are not provided", () => {
    const result = problem3();
    expect(result).deep.equal(expectedNoMatchResult);
  });
});
