const expect = require("chai").expect;
const inventory = require("../inventory.js");
const problem1 = require("../problem1");

const id_key = 14;
const expectedMatchResult = [
  { id: id_key, car_make: "Dodge", car_model: "Ram Van 1500", car_year: 1999 },
];
const expectedNoMatchResult = [];
describe("Test problem #1", () => {
  it("returns vehicle details when correct arguments are provided", () => {
    const result = problem1(inventory, id_key);
    expect(result).deep.equal(expectedMatchResult);
  });
  it("returns an empty array when inventory is empty", () => {
    const result = problem1([], id_key);
    expect(result).deep.equal(expectedNoMatchResult);
  });
  it("returns an empty array when key provided is invalid", () => {
    const result = problem1(inventory, "hello");
    expect(result).deep.equal(expectedNoMatchResult);
  });
  it("returns an empty array when inventory is not and array of objects", () => {
    const result = problem1({ hello: "car" }, id_key);
    expect(result).deep.equal(expectedNoMatchResult);
  });
  it("returns an empty array when inventory array has invalid object", () => {
    const result = problem1([{ hello: "car" }], id_key);
    expect(result).deep.equal(expectedNoMatchResult);
  });
  it("returns an empty array when arguments are not provided", () => {
    const result = problem1();
    expect(result).deep.equal(expectedNoMatchResult);
  });
  it("returns an empty array when no match is found", () => {
    const result = problem1(inventory, 0);
    expect(result).deep.equal(expectedNoMatchResult);
  });
});
