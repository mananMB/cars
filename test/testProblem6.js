const problem6 = require("../problem6");
const inventory = require("../inventory.js");
const expect = require("chai").expect;

let vehicleMakeArray = ["BMW", "Audi"];

const expectedMatchResult = JSON.stringify([
  { id: 25, car_make: "BMW", car_model: "525", car_year: 2005 },
  { id: 30, car_make: "BMW", car_model: "6 Series", car_year: 2010 },
  { id: 8, car_make: "Audi", car_model: "4000CS Quattro", car_year: 1987 },
  { id: 44, car_make: "Audi", car_model: "Q7", car_year: 2012 },
  { id: 6, car_make: "Audi", car_model: "riolet", car_year: 1995 },
  { id: 45, car_make: "Audi", car_model: "TT", car_year: 2008 },
]);

const expectedNoMatchResult = JSON.stringify([]);
const expectedResultWithInvalidArguments = [];
describe("Test problem #6", () => {
  it("returns matched values when valid arguments are provided", () => {
    const result = problem6(inventory, vehicleMakeArray);
    expect(result).equal(expectedMatchResult);
  });
  it("returns '[]' when no match is found", () => {
    const result = problem6(inventory, ["hadjakjdgello"]);
    expect(result).equal(expectedNoMatchResult);
  });
  it("returns empty array when invalid arguments provided", () => {
    const result = problem6({ inventory }, ["hadjakjdgello"]);
    expect(result).deep.equal(expectedResultWithInvalidArguments);
  });
});
