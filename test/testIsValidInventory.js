const isValidInventory = require("../isValidInventory");
const inventory = require("../inventory");
const expect = require("chai").expect;

describe("Test if inventory provided is valid", () => {
  it("returns true if inventory is valid", () => {
    const result = isValidInventory(inventory);
    expect(result).to.be.true;
  });
  it("returns false if inventory provided is not an array of objects", () => {
    const result = isValidInventory(inventory[0]);
    expect(result).to.be.false;
  });
});
