const inventory = require("../inventory.js");
const problem4 = require("../problem4");
const problem5 = require("../problem5");
const { expect } = require("chai");

let year = 2000;
const expectedMatchResult = 25;
const expectedNoMatchResult = [];
describe("Test problem #5", () => {
  it("returns cars made before year specified", () => {
    const result = problem5(problem4(inventory), year);
    expect(result).to.be.equal(expectedMatchResult);
  });
  it("returns an empty array when array of years is empty", () => {
    const result = problem5([], year);
    expect(result).deep.equal(expectedNoMatchResult);
  });
  it("returns an empty array when year is not provided", () => {
    const result = problem5(problem4(inventory));
    expect(result).deep.equal(expectedNoMatchResult);
  });
  it("returns an empty array when array of years is not provided", () => {
    const result = problem5(year);
    expect(result).deep.equal(expectedNoMatchResult);
  });
  it("returns an empty array when inventory is an array of numbers", () => {
    const result = problem5({ hello: "car" }, year);
    expect(result).deep.equal(expectedNoMatchResult);
  });
  it("returns an empty array when inventory array has invalid object", () => {
    const result = problem5([{ hello: "car" }], year);
    expect(result).deep.equal(expectedNoMatchResult);
  });
  it("returns an empty array when year provided is not an integer", () => {
    const result = problem5([{ hello: "car" }], "Hello");
    expect(result).deep.equal(expectedNoMatchResult);
  });
  it("returns an empty array when arguments are not provided", () => {
    const result = problem5();
    expect(result).deep.equal(expectedNoMatchResult);
  });
});
