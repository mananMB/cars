const expect = require("chai").expect;
const problem4 = require("../problem4");
const inventory = require("../inventory.js");

const expectedMatchResult = [
  2000, 1987, 2005, 2010, 1983, 2005, 1994, 1992, 1997, 1995, 2010, 2000, 1998,
  2005, 1991, 2004, 2012, 2003, 1985, 2009, 2004, 1990, 1964, 2000, 2007, 1997,
  2008, 2001, 2001, 2003, 1965, 2009, 1995, 2000, 2012, 2009, 1999, 1999, 1995,
  1996, 1987, 1992, 1993, 1999, 2008, 1996, 1997, 2011, 2003, 1996,
];
const expectedNoMatchResult = [];
describe("Test problem #4", () => {
  it("returns array of years of model of all inventory cars", () => {
    const result = problem4(inventory);
    expect(result).deep.equal(expectedMatchResult);
  });
  it("returns an empty array when inventory is empty", () => {
    const result = problem4([]);
    expect(result).deep.equal(expectedNoMatchResult);
  });
  it("returns an empty array when inventory is not and array of objects", () => {
    const result = problem4({ hello: "car" });
    expect(result).deep.equal(expectedNoMatchResult);
  });
  it("returns an empty array when inventory array has invalid object", () => {
    const result = problem4([{ hello: "car" }]);
    expect(result).deep.equal(expectedNoMatchResult);
  });
  it("returns an empty array when arguments are not provided", () => {
    const result = problem4();
    expect(result).deep.equal(expectedNoMatchResult);
  });
});
