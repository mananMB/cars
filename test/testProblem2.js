const expect = require("chai").expect;
const problem2 = require("../problem2");
const inventory = require("../inventory.js");

const expectedMatchResult = [
  { id: 50, car_make: "Lincoln", car_model: "Town Car", car_year: 1999 },
];
const expectedNoMatchResult = [];
describe("Test problem #2", () => {
  it("returns details of last vehicle in inventory when correct arguments are provided", () => {
    const result = problem2(inventory);
    expect(result).deep.equal(expectedMatchResult);
  });
  it("returns an empty array when inventory is empty", () => {
    const result = problem2([]);
    expect(result).deep.equal(expectedNoMatchResult);
  });
  it("returns an empty array when inventory is not and array of objects", () => {
    const result = problem2({ hello: "car" });
    expect(result).deep.equal(expectedNoMatchResult);
  });
  it("returns an empty array when inventory array has invalid object", () => {
    const result = problem2([{ hello: "car" }]);
    expect(result).deep.equal(expectedNoMatchResult);
  });
  it("returns an empty array when arguments are not provided", () => {
    const result = problem2();
    expect(result).deep.equal(expectedNoMatchResult);
  });
});
