// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory.
// Execute a function to find what the make and model of the last car in the inventory is?
// Log the make and model into the console in the format of:
// "Last car is a *car make goes here* *car model goes here*"

const isValidInventory = require("./isValidInventory");
const problem2 = (inventory) => {
  const getLastVehicleInInventory = (inventory) => {
    let lastID = -1;
    for (let i = 0; i < inventory.length; i++) {
      if (inventory[i].id > lastID) {
        lastID = inventory[i].id;
      }
    }
    return lastID;
  };

  const findVehicleByID = (inventory, id) => {
    for (let i = 0; i <= inventory.length; i++) {
      if (inventory[i].id === id) {
        return inventory[i];
      }
    }
  };

  const printLastVehicleDetails = (inventory) => {
    let vehicle = findVehicleByID(
      inventory,
      getLastVehicleInInventory(inventory)
    );
    return [vehicle];
  };

  if (isValidInventory(inventory)) {
    return printLastVehicleDetails(inventory);
  }

  return [];
};

module.exports = problem2;
