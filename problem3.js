// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website.
// Execute a function to Sort all the car model names into alphabetical order
// and log the results in the console as it was returned.

const isValidInventory = require("./isValidInventory");
const problem3 = (inventory) => {
  const swapArrayElements = (array, i, j) => {
    let temporary;

    temporary = array[j];
    array[j] = array[i];
    array[i] = temporary;

    return array;
  };

  const sortInventoryByCarModel = (inventory) => {
    for (let i = 0; i < inventory.length; i++) {
      for (let j = i + 1; j < inventory.length; j++) {
        if (
          inventory[i].car_model.toLowerCase() >
          inventory[j].car_model.toLowerCase()
        ) {
          inventory = swapArrayElements(inventory, i, j);
        }
      }
    }
    return inventory;
  };

  if (isValidInventory(inventory)) {
    if (inventory.length === 1) {
      return inventory;
    }
    return sortInventoryByCarModel(inventory);
  }

  return [];
};

module.exports = problem3;
