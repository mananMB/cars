// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.
// Execute a function and return an array that only contains BMW and Audi cars.
// Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

const isValidInventory = require("./isValidInventory");
const problem6 = (inventory, vehicleMakeArray) => {
  const vehiclesOfMake = (inventory, vehicleMakeArray) => {
    let selectedMakes = [];

    for (let i = 0; i < vehicleMakeArray.length; i++) {
      for (let j = 0; j < inventory.length; j++) {
        if (
          vehicleMakeArray[i].toString().toLowerCase() ===
          inventory[j].car_make.toLowerCase()
        ) {
          selectedMakes.push(inventory[j]);
        }
      }
    }

    return selectedMakes;
  };

  if (
    isValidInventory(inventory) &&
    Array.isArray(vehicleMakeArray) &&
    vehicleMakeArray.length !== 0
  ) {
    return JSON.stringify(vehiclesOfMake(inventory, vehicleMakeArray));
  }

  return [];
};

module.exports = problem6;
