const inventoryObjectKeys = ["id", "car_make", "car_model", "car_year"];

const isValidInventory = (inventory) => {
  if (Array.isArray(inventory) && inventory.length !== 0) {
    for (let i = 0; i < inventory.length; i++) {
      if (
        Object.keys(inventory[i]).every((key) =>
          inventoryObjectKeys.includes(key)
        )
      ) {
        return true;
      }
    }
  }
  return false;
};

module.exports = isValidInventory;
