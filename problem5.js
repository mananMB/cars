// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000.
// Using the array you just obtained from the previous problem, find out how many cars
// were made before the year 2000 and return the array of older cars and log its length.

const problem5 = (arrayOfYears, year) => {
  const checkYear = (carYear) => {
    return carYear < year;
  };

  const findCarsMadeBefore = (arrayOfYears) => {
    return arrayOfYears.filter(checkYear).length;
  };

  const isValidArrayOfIntegers = (arrayOfYears) => {
    if (Array.isArray(arrayOfYears) && arrayOfYears.length !== 0) {
      return arrayOfYears.every((year) => {
        return Number.isInteger(year);
      });
    }
    return false;
  };

  if (isValidArrayOfIntegers(arrayOfYears) && Number.isInteger(year)) {
    return findCarsMadeBefore(arrayOfYears);
  }

  return [];
};

module.exports = problem5;
